libmath-prime-util-gmp-perl (0.51-2) UNRELEASED; urgency=medium

  * Use system build dependencies instead of embedded modules in inc/

 -- Clément Hermann <nodens@nodens.org>  Sat, 20 Jul 2019 17:26:25 -0300

libmath-prime-util-gmp-perl (0.51-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian wrong-path-for-interpreter error

  [ gregor herrmann ]
  * Import upstream version 0.51.
  * Declare compliance with Debian Policy 4.2.1.

 -- gregor herrmann <gregoa@debian.org>  Mon, 10 Sep 2018 00:55:53 +0200

libmath-prime-util-gmp-perl (0.50-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.50.
  * Declare compliance with Debian Policy 4.1.3.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Tue, 02 Jan 2018 23:36:54 +0100

libmath-prime-util-gmp-perl (0.48-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.48.

 -- gregor herrmann <gregoa@debian.org>  Mon, 06 Nov 2017 00:13:28 +0100

libmath-prime-util-gmp-perl (0.47-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.47.
    Fixes "libmath-prime-util-gmp-perl FTBFS on big endian: Failed 2/31
    test programs. 8/2885 subtests failed." (Closes: #870832)
  * Bump versioned Recommends on libmath-prime-util-perl to >= 0.68.
  * Declare compliance with Debian Policy 4.1.1.

 -- gregor herrmann <gregoa@debian.org>  Sun, 05 Nov 2017 18:36:59 +0100

libmath-prime-util-gmp-perl (0.46-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.46.
  * Update years of upstream copyright.
  * Bump recommended version of libmath-prime-util-perl.
  * Declare compliance with Debian Policy 4.0.0.

 -- gregor herrmann <gregoa@debian.org>  Fri, 04 Aug 2017 17:42:28 -0400

libmath-prime-util-gmp-perl (0.41-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Remove Daniel Kahn Gillmor from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Import upstream version 0.41
  * debian/NEWS: Document API changes for new upstream version 0.41
  * Add myself to Uploaders.
    Add myself to Uploaders since the last Uploader remaining (thanks Daniel
    Kahn Gillmor for your previous work) asked to be removed.
  * Add myself to copyright stanza for debian/* packaging

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 11 Oct 2016 16:37:38 +0200

libmath-prime-util-gmp-perl (0.40-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Import upstream version 0.40
  * Update copyright years for upstream files
  * Declare compliance with Debian policy 3.9.8
  * debian/rules: Build enabling all hardening flags

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 05 Aug 2016 14:40:27 +0200

libmath-prime-util-gmp-perl (0.35-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.35.

 -- gregor herrmann <gregoa@debian.org>  Tue, 15 Dec 2015 20:00:20 +0100

libmath-prime-util-gmp-perl (0.34-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.34

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Fri, 23 Oct 2015 19:20:05 -0200

libmath-prime-util-gmp-perl (0.33-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.33

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Tue, 15 Sep 2015 17:30:11 -0300

libmath-prime-util-gmp-perl (0.32-1) unstable; urgency=medium

  * Team upload.

  [ Lucas Kanashiro ]
  * Import upstream version 0.32
  * Update upstream copyright

  [ gregor herrmann ]
  * Bump version of libmath-prime-util-perl in Recommends.

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Wed, 02 Sep 2015 22:46:53 -0300

libmath-prime-util-gmp-perl (0.31-1) unstable; urgency=medium

  * Team upload.

  [ Partha Pratim Mukherjee ]
  * Import upstream version 0.29

  [ gregor herrmann ]
  * Import upstream version 0.31
  * Update years of upstream copyright.
  * Make Recommends on libmath-prime-util-perl versioned.

 -- gregor herrmann <gregoa@debian.org>  Sat, 27 Jun 2015 19:13:48 +0200

libmath-prime-util-gmp-perl (0.27-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.27
  * Drop manpage-has-errors-from-pod2man.patch, merged upstream.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Wed, 15 Oct 2014 19:48:20 +0200

libmath-prime-util-gmp-perl (0.26-1) unstable; urgency=medium

  * Team upload.
  * Imported upstream version 0.26
  * Declare compliance with Debian Policy 3.9.6
  * Make Recommends on libmath-prime-util-perl unversioned
  * Add manpage-has-errors-from-pod2man.patch patch

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 02 Oct 2014 23:25:50 +0200

libmath-prime-util-gmp-perl (0.23-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Imported upstream version 0.23

 -- gregor herrmann <gregoa@debian.org>  Mon, 15 Sep 2014 20:55:37 +0200

libmath-prime-util-gmp-perl (0.21-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Fri, 20 Jun 2014 17:14:18 +0200

libmath-prime-util-gmp-perl (0.20-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Make Recommends on libmath-prime-util-perl versioned.

 -- gregor herrmann <gregoa@debian.org>  Thu, 19 Jun 2014 17:33:01 +0200

libmath-prime-util-gmp-perl (0.19-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * Drop version constraint from libmath-prime-util-perl in Recommends.
    Nothing older in the archive.

 -- gregor herrmann <gregoa@debian.org>  Wed, 07 May 2014 20:38:11 +0200

libmath-prime-util-gmp-perl (0.18-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Tue, 04 Feb 2014 21:47:07 +0100

libmath-prime-util-gmp-perl (0.17-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop spelling.patch, merged upstream.
  * Update years of upstream copyright.
  * Bump versioned Recommends on libmath-prime-util-perl to >= 0.37.

 -- gregor herrmann <gregoa@debian.org>  Sun, 26 Jan 2014 16:27:38 +0100

libmath-prime-util-gmp-perl (0.16-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Add years of upstream copyright.
  * Add debian/NEWS, mentioning an API change.
  * Make Recommends on libmath-prime-util-perl versioned.
  * Slightly improve short and long description.
  * Use debhelper 9.20120312 to get all hardening flags.
  * Use canonical URL in Vcs-Git field.
  * Install example scripts.
  * Add a patch to fix a spelling mistake.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 21 Dec 2013 22:30:22 +0100

libmath-prime-util-gmp-perl (0.07-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Daniel Kahn Gillmor ]
  * new upstream version.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 21 Mar 2013 18:20:52 -0400

libmath-prime-util-gmp-perl (0.6-1) unstable; urgency=low

  * Initial Release. (Closes: #696309)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 19 Dec 2012 01:54:58 -0500
