Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Math-Prime-Util-GMP
Source: https://metacpan.org/release/Math-Prime-Util-GMP
Upstream-Contact: Dana A Jacobsen <dana@acm.org>

Files: *
Copyright: 2011-2017, Dana A Jacobsen <dana@acm.org>
License: Artistic or GPL-1+

Files: inc/Devel/CheckLib.pm
Copyright: 2007 David Cantrell, David Golden
License: Artistic or GPL-1+

Files: xt/expr.h xt/expr.c xt/expr-impl.h
Copyright: 2000-2002, 2004 Free Software Foundation, Inc.
License: LGPL-3+ or GPL-2+

Files: debian/*
Copyright: 2012, Daniel Kahn Gillmor <dkg@fifthhorseman.net>
 2015, Lucas Kanashiro <kanashiro.duarte@gmail.com>
 2016, Salvatore Bonaccorso <carnil@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; version 3.
 .
 On Debian systems, the complete text of version 3 of the GNU Lesser
 General Public License can be found in `/usr/share/common-licenses/LGPL-3'.
